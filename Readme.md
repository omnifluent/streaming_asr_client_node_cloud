# Apptek Transcribe Streaming Service Client For Apptek Cloud

Assuming Node.js is installed.

1. Requires Apptek license key.
2. Install required packages (pacakge.json)
3. Place .env file with the options provided at project root or the options provided can be chained with run command.
4. If you provide .env file run 

    `node streaming.js`
    
    or 
    
    `LICENSE_KEY=< License provided by Apptek > source_lang_code=< Language code > encoding=< encoding > sample_rate=< Sampling Rate > domain=default audio_file=< Path to Audio File > node streaming.js`

ENV File:

    LICENSE_KEY=<License provided by Apptek>
    source_lang_code=<Language code>
    encoding=<encoding>
    sample_rate=<Sampling Rate>
    domain=default
    audio_file=<Path to Audio File>
    target_lang_code=<Translation language code depending on source language code> (optional)

Available Languages and Options:

    To get available source languages and translation language pairs use "getAvailable.js". This shall save a file "languages.json" to this project directory.
    

What this app does: 

when audio binary is ready to send

1. Get the session token and Gate way Url from license server using Key provided in .env file.
2. Initialize a bidirectional streaming RPC.
3. Send in configuration message(`apptek.proto`) as first packet with options in env file.
4. Continue to send audio subsequently with out delay greater than 15sec (if no packest were sent for 15 seconds, service throws an error and connection will be terminated)
5. When sending and receiving data is done close the bidirectional stream. 
