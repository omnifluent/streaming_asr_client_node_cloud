"use strict";

require('dotenv').config({path: './.env'})
const fs = require('fs');

// getting instance of a transcribe service.
const transcribeService = new(require("./transcription_service.js"))();

function saveLanguages(data, cb) {
  fs.writeFile(`./languages.json`, JSON.stringify(data), 'utf-8', function (err) {
    if (err) {
      console.log(err);
      return cb(err, null);
    } else {
      console.log('Wrote to file');
      return cb(null, 'Done');
    }
  });
}

function availableLanguages() {
  const transcribeService = new(require("./transcription_service.js"))();
  transcribeService.getAvailableLanguages((err, message) => {
    if (err) {
      console.log(err);
      process.exit();
    } else {
      saveLanguages(message, (writeErr) => {
        if (writeErr) {
          console.log(writeErr);
        } else {
          console.log('languages json file saved');
        }
        process.exit();
      });
    }
  });
}


availableLanguages();

// To keep node process from terminating. 
// Press ctrl + c to exit.
setInterval((function() {
  return;
}), 10000);