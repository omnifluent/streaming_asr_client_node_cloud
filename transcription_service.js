function transcribeClient() {

    const grpc = require('grpc');
    const protoLoader = require('@grpc/proto-loader');
    const path = require('path');
    const fs = require('fs');
    const request = require('request');

    // .proto file location 
    const PROTO_PATH = path.join(__dirname, './apptek.proto');

    var audio_config = {
        encoding: process.env.encoding, // encoding of the audio data
        sample_rate_hz: parseInt(process.env.sample_rate), // sample rate of the audio data in Hz
        lang_code: process.env.source_lang_code, // language code of the audio
        domain: process.env.domain, // domain name (leave empty if not sure)
        target_lang_code: process.env.target_lang_code // language code for translation 
    }

    // key from env
    const key = process.env.LICENSE_KEY;

    var client, token, audioCall;

    const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    });
    const packageDescriptor = grpc.loadPackageDefinition(packageDefinition);
    const gateway = packageDescriptor.gateway;

    const decodingJWT = (token) => {
        if (token !== null || token !== undefined) {
            const base64String = token.split('.')[1];
            const decodedValue = JSON.parse(Buffer.from(base64String,
                'base64').toString('ascii'));
            return decodedValue;
        }
        return null;
    }

    this.initiateConnection = function (cb) {
        request.get(`https://license.apptek.com/license/v2/token/${key}`, {
            timeout: 1500
        }, (err, res, body) => {
            if (err) {
                cb(err, null);
            } else if (res.statusCode == 200) {
                try {
                    // var jsonObj = JSON.parse(body);
                    var jsonObj = decodingJWT(res.body);
                    console.log(JSON.stringify(jsonObj));
                    token = res.body;
                    const credentials = grpc.credentials.createSsl();
                    client = new gateway.Gateway(`${jsonObj.host}:${jsonObj.port}`, credentials);
                    cb(null, token);
                } catch (error) {
                    cb(error, null);
                }
            } else {
                console.log(JSON.stringify(res));
                cb('unknown Exception', null);
            }
        })
    }

    this.streamAudio = function (data, cb) {
        var messageAudio = {
            // oneof_request : {
            audio: data.audio
            // }
        }
        if (audioCall) {
            audioCall.write(messageAudio);
        } else {
            var configuration = {

                license_token: token,
                audio_configuration: {
                    encoding: audio_config.encoding,
                    sample_rate_hz: audio_config.sample_rate_hz,
                    lang_code: audio_config.lang_code,
                }
            }
            if (audio_config.target_lang_code) {
                configuration.translate_configuration = {
                    translate_interval: 0,
                    target_lang_code: audio_config.target_lang_code
                }
            }
            var config = {
                // oneof_request : {
                configuration: configuration
                // }
            }
            audioCall = client.RecognizeStream();
            audioCall.on('data', function (data) {
                cb(null, data);
            });
            audioCall.on('end', function () {
                console.log("Connection Closed");
                process.exit();
            });
            audioCall.on('error', function (e) {
                cb(e);
            });
            audioCall.write(config);
            audioCall.write(messageAudio);
        }
    };

    this.endAudioCall = function () {
        if (audioCall) {
            audioCall.end();
            audioCall = null;
        }
    }

    this.getAvailableLanguages = function (cb) {
        request.get(`https://license.apptek.com/license/v2/token/${key}`, {
            timeout: 3000
        }, (err, res, body) => {
            if (err) {
                console.log(err);
                cb(err, null);
            } else if (res.statusCode == 200) {
                try {
                    var jsonObj = decodingJWT(res.body);
                    token = res.body;
                    const credentials = grpc.credentials.createSsl();
                    const langClient = new gateway.Gateway(`${jsonObj.host}:${jsonObj.port}`, credentials);
                    langClient.RecognizeGetAvailable({
                        license_token: token
                    }, (rpcErr, message) => {
                        if (rpcErr) {
                            cb(rpcErr, null);
                            return;
                        } else {
                            cb(null, message);
                        }
                    });
                } catch (error) {
                    console.log(error);
                    cb(error, null);
                    return;
                }
            } else {
                cb('Unknown exception', null);
                return;
            }
        });
    }
}

module.exports = transcribeClient;