"use strict";

require('dotenv').config({path: './.env'})
const fs = require('fs');

// getting instance of a transcribe service.
const transcribeService = new(require("./transcription_service.js"))();
var audioBuffer = [];
let sessionToken = '';

// adding a simple wait just to simulate streaming use case
function wait(milleseconds) {
    return new Promise(resolve => setTimeout(resolve, milleseconds))
}

// sending audio packets 
async function sendAudioToBot() {
    let stopStream = false;
    for (let i = 0; i < audioBuffer.length; i++) {
        // fake delay of 1sec to simulate streaming.
        await wait(2048/(8000 * 2));
        if (stopStream) {
            console.log('Exiting . . .');
            process.exit();
        }
        if(i == 0 ){
            console.log('config sent');
        }
        console.log('packet sent');
        var data = {
            audio: audioBuffer[i]
        }
        // sending audio and 
        transcribeService.streamAudio(data, function(err, data){
            if(err){
                console.log(err);
                stopStream = true;
            }else{
                // unncomment if you want to see all the messages.
                console.log(JSON.stringify(data))
                // if(data.oneof_response && data.oneof_response == 'transcription'){
                //     if(data.transcription.orth){
                //         console.log('" ',data.transcription.orth, ' "');
                //     }
                // }
            }
        });

        if(i >= audioBuffer.length - 1){
            console.log("Audio streaming complete");
            transcribeService.endAudioCall();
            // if (sessionToken) {
            //     transcribeService.releaseToken(sessionToken, (err, msg) => {
            //         if (err) {
            //             console.log('err from releasing token', err);
            //         } else {
            //             console.log('Token released');
            //         }
            //     })
            // }
        }
    }
};

function readFile(){
    var fileName = process.env.audio_file;
    var readStream = fs.createReadStream(fileName);
    readStream.on('data', function (chunk) {
        // console.log(chunk);
        audioBuffer.push(chunk);
    });
    readStream.on('end', function(){
        console.log('File read to buffer');
    });
    readStream.on('error', function(err) {
        console.log(err);
    });
}

transcribeService.initiateConnection((err, msg) => {
    if(err){
        console.log(err);
        console.log('Exiting . . .');
        process.exit();
    }else{
        console.log('Service Initiated. ');
        console.log('Session Token', msg);
        sessionToken = msg;
        readFile();
        setTimeout(() => {
            sendAudioToBot();
        }, 1000);
    }
});

// To keep node process from terminating. 
// Press ctrl + c to exit.
setInterval((function() {
    return;
}), 10000);
